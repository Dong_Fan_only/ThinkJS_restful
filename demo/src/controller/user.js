const BaseRest = require('./rest.js');
const fs=require("fs");
module.exports = class extends BaseRest {
  async getAction() {
    await super.getAction();
  }

  async postAction(){
    if (this.isGet){
      return this.display("addUser.html")
    }else{
     try{
       await this.model("user").add({
         id:think.uuid("v5"),
         username:this.post().username,
         password:think.md5(this.post().password)
       });
       this.success("注册成功");
     }catch (e) {
       this.fail("添加失败,原因:"+e.toString());
     }
    }
  }

  async deleteAction(){
    await super.deleteAction();
  }

  async putAction(){
    if (this.isGet){
      console.log(this.id);
      const editUser=await this.model("user").where({id:this.id}).find();
      this.assign("editUser",editUser);
      return this.display("editUser.html");
    }else{
      try {
        const affdoc=await this.model("user").where({id:this.post().id}).update({
          username:this.post().username,
          password:think.md5(this.post().password)
        })
        this.success("修改记录:"+affdoc);
      }catch (e) {
        this.fail("修改失败,原因:"+e.toString());
      }
    }
  }

  async loginAction(){
    if (this.isGet){
      return this.display("userLogin.html");
    }else{
      const loginUser=await this.model("user").where({username:this.post().username}).find();
      if (think.isEmpty(loginUser)) return this.fail("该用户未被注册");
      else if(loginUser.password!==think.md5(this.post().password)) return this.fail("密码错误");
      else this.body="<h1>欢迎用户"+loginUser.username+"于"+new Date().toLocaleString()+"登录</h1>";
    }
  }

  async uploadAction(){
    if (this.isGet){
      return this.display("upload.html");
    }else{
      try {
        const file=this.file("file");
        const data=fs.readFileSync(file.path,"binary");
        think.mkdir(think.ROOT_PATH+"/www/static/upload");
        fs.writeFileSync(think.ROOT_PATH+"/www/static/upload/"+file.name,data,"binary");
      }catch (e){
        this.fail("上传失败,原因:"+e.toString());
      }
    }
  }
};
